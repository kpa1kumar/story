package com.zycus.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

import com.zycus.integration.controller.UserController;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

@Component
public class VertxServerVerticle extends AbstractVerticle {

	@Autowired
	private ApplicationConfiguration applicationConfiguration;
	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	private UserController uc;

	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		super.stop(stopFuture);
		System.out.println("MyVerticle stopped! @ Future Stop");
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		System.out.println("MyVerticle stopped! @ Stop");
	}

	@Override
	public void start() throws Exception {
		super.start();
		System.out.println("MyVerticle started! @ Start");
		vertx.createHttpServer().requestHandler(router()::accept).listen(applicationConfiguration.httpPort());
	}

	private Router router() {
		Router router = Router.router(vertx);
		router.route("/pavankumar").handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response.putHeader("Content-Type", "application/json");
			response.end(Json.encodePrettily(discoveryClient.getInstances(applicationConfiguration.applicationName())));
		});
		router.get("/users").handler(this::getAll);
		// router.get("/age/:age1/:age2").handler(this::getByAgeInRange);
		System.out.println("Route");
		return router;
	}

	private void getAll(RoutingContext routingContext) {
		routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encodePrettily(uc.users()));
	}

	/*
	 * private void getByAgeInRange(RoutingContext routingContext) {
	 * routingContext.response() .putHeader("content-type",
	 * "application/json; charset=utf-8")
	 * .end(Json.encodePrettily(uc.getByAgeInRange(routingContext.request().getParam
	 * ("age1"),routingContext.request().getParam("age2")))); }
	 */
	/*
	 * @Override public void start() { Router router = Router.router(vertx);
	 * 
	 * router.route().handler(BodyHandler.create());
	 * router.get("/users").handler(this::users);
	 * vertx.createHttpServer().requestHandler(router::accept).listen(
	 * applicationConfiguration.httpPort()); }
	 * 
	 * public List<User> users(RoutingContext context) {
	 * System.out.println("Cheking Routing"); HttpServerResponse response =
	 * context.response(); response.putHeader("Content-Type", "application/pavan");
	 * response.end(Json.encodePrettily(discoveryClient.getInstances(
	 * applicationConfiguration.applicationName()))); return use.allUsers(); }
	 */
}
