package com.zycus.application;

import io.vertx.core.Vertx;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.jms.annotation.EnableJms;

@EnableDiscoveryClient
@EnableJms
@SpringBootApplication(scanBasePackages = { "com.zycus.integration.controller", "com.zycus.integration.exception",
											"com.zycus.integration.jms", "com.zycus.integration.model", "com.zycus.integration.service",
											"com.zycus.integration.dao", "com.zycus.Application" })
@EntityScan(basePackages = { "com.zycus.integration.model" })
public class UserApplication {

	@Autowired
	private VertxServerVerticle vertxServerVerticle;

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class);
	}

	@PostConstruct
	public void deployServerVerticle() {
		Vertx.vertx().deployVerticle(vertxServerVerticle);
	}
}
