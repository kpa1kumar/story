package com.zycus.integration.exception;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@Autowired
	private JmsTemplate jmsteplate;

	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException occured")
	@ExceptionHandler(IOException.class)
	public ErrorMessage handleIOException(final Exception ex) {
		return new ErrorMessage(400, ex.getMessage());
	}

	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorMessage handleUserNotFoundException(final UserNotFoundException ex) {
		return new ErrorMessage(404, "The user was not found", "Please try again Later..! :)");
	}

	@ExceptionHandler(UserAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ErrorMessage handleUserAlreadyExistsException(final UserAlreadyExistsException ex) {
		return new ErrorMessage(200, "The User Already Exists..!", "Duplicate Content Not Allowed:)");
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Throwable.class)
	@ResponseBody
	public ErrorMessage handleThrowable(final Throwable ex) {
		return new ErrorMessage(500, "An unexpected internal server error occured");
	}

	@ExceptionHandler(value = Exception.class)
	public ErrorMessage handleException(Exception e) {
		System.out.println(".............." + e.getMessage());
		jmsteplate.convertAndSend("PavanError", e.getMessage());
		return new ErrorMessage(500, e.getMessage(), "Please try again Later..! :)");
	}
}
