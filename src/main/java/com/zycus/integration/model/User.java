package com.zycus.integration.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users")
@NamedQueries({
		/*
		 * @NamedQuery( name="getByAge", query="FROM User WHERE age =:age" ),
		 */
		@NamedQuery(name = "getAgeByRange", query = "FROM User WHERE age BETWEEN :age1 AND :age2") })
public class User implements Serializable {// extends ResourceSupport {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private int iden;

	@Column
	@NotNull
	private String name;

	@Column
	@NotNull
	private int age;
	@Column
	@NotNull
	private String dept;

	@Column
	private int hashcode;

	// @OneToMany
	// private List<Link> links = new ArrayList<>();

	public int getHashcode() {
		return hashcode;
	}

	public void setHashcode(int hashcode) {
		this.hashcode = hashcode;
	}

	public User() {
	}

	public User(int iden) {
		this.iden = iden;
	}

	public User(int iden, String name, int age, String dept) {
		super();
		this.iden = iden;
		this.name = name;
		this.age = age;
		this.dept = dept;
		// add(linkTo(UserController.class).withRel("self"));
	}

	public int getIden() {
		return iden;
	}

	public void setIden(int iden) {
		this.iden = iden;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	/*
	 * public void addLink(String url, String rel) { Link link = new Link();
	 * link.setLink(url); link.setRel(rel); links.add(link); }
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((dept == null) ? 0 : dept.hashCode());
		result = prime * result + iden;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (age != other.age)
			return false;
		if (dept == null) {
			if (other.dept != null)
				return false;
		} else if (!dept.equals(other.dept))
			return false;
		if (iden != other.iden)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [iden=" + iden + ", name=" + name + ", age=" + age + ", dept=" + dept + "]";
	}
}
