package com.zycus.integration.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.zycus.integration.dao.UserDAO;
import com.zycus.integration.model.User;

@Service
@Primary
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO use;

	@Override
	public List<User> listAll() {
		return use.allUsers();
	}

	@Override
	public User getById(int iden) {
		return use.getUserById(iden);
	}

	@Override
	public void save(User user) {
		use.create(user);
	}

	@Override
	public void update(User user) {
		use.update(user);
	}

	@Override
	public void delete(User user) {
		use.deleteUserById(user);
	}

	@Override
	public List<User> getByAge(int age) {
		return use.getUserByAge(age);
	}

	@Override
	public List<User> getByAgeInRange(int age1, int age2) {
		return use.getAgeInRange(age1, age2);
	}

}
