package com.zycus.integration.service;

import com.zycus.integration.model.User;

import java.util.List;

public interface UserService {

	List<User> listAll();

	User getById(int id);

	void save(User user);

	void update(User user);

	void delete(User user);

	List<User> getByAge(int age);

	List<User> getByAgeInRange(int age1, int age2);

}
