package com.zycus.integration.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class PavanErrorConsumer {

	@JmsListener(destination = "PavanError")
	public void receiveMessage(final String message) throws InterruptedException {
		System.out.println("PavanErrorConsumer: " + message);
	}
}
