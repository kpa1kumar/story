package com.zycus.integration.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.zycus.integration.model.User;

@Component
public class Consumer {

	@JmsListener(destination = "PavanQueue")
	public void receiveMessage(final String message) throws InterruptedException {
		System.out.println("Receiver:" + message);
	}

	@JmsListener(destination = "PavanObjQueue")
	public void receiveMessage2(User user) throws InterruptedException {
		System.out.println("Receiver:" + user);
	}
}
