package com.zycus.integration.dao;

import java.util.List;

import com.zycus.integration.model.User;

public interface UserDAO {

	public void create(User user);

	public List<User> allUsers();

	public User getUserById(int iden);

	public void update(User user);

	public void deleteUserById(User user);

	public List<User> getUserByAge(int age);

	public List<User> getAgeInRange(int age1, int age2);

}
