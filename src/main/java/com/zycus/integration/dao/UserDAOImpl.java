package com.zycus.integration.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zycus.integration.exception.UserAlreadyExistsException;
import com.zycus.integration.exception.UserNotFoundException;
import com.zycus.integration.model.User;

@Repository
@Transactional
@Primary
public class UserDAOImpl implements UserDAO {

	@PersistenceContext
	private EntityManager entityManager;

	private boolean isUserExists(User user) {
		boolean flag = false;
		if (entityManager.find(User.class, user.getIden()) != null) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

	private boolean isUserHashCodeExists(User user) {
		boolean flag = false;
		Object result = entityManager
				.createQuery("select count(hashcode) from User where hashcode=" + user.getHashcode()).getSingleResult();
		if (result.equals(1) || isUserExists(user)) {
			flag = true;
			System.out.println(flag + " from 1");
		} else {
			System.out.println(flag + " from else");
			flag = false;
		}
		return flag;
	}

	/* Insert operation from Queue Asynchronous */
	// @JmsListener(destination = "InsertQueue")
	public void SaveUser(User user) {
		if (isUserHashCodeExists(user)) {
			System.out.println("hashcode exists");
			throw new UserAlreadyExistsException();
		} else {
			entityManager.persist(user);
		}
	}

	public void create(User user) {
		if (isUserHashCodeExists(user)) {
			System.out.println("hashcode exists");
			throw new UserAlreadyExistsException();
		} else {
			entityManager.persist(user);
		}
	}

	/* Delete operation from Queue Asynchronous */
	// @JmsListener(destination = "DeleteQueue")
	public void DeleteUser(User user) {
		if (isUserExists(user)) {
			entityManager.remove(entityManager.contains(user) ? user : entityManager.merge(user));
		} else {
			throw new UserNotFoundException();
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> allUsers() {
		return entityManager.createQuery("from User").getResultList();
	}

	public User getUserById(int iden) {
		if (entityManager.find(User.class, iden) == null) {
			throw new UserNotFoundException();
		}
		return entityManager.find(User.class, iden);
	}

	public void update(User user) {
		entityManager.merge(user);
	}

	public void deleteUserById(User user) {
		if (isUserExists(user)) {
			entityManager.remove(entityManager.contains(user) ? user : entityManager.merge(user));
		} else {
			throw new UserNotFoundException();
		}
		/*
		 * if (entityManager.contains(user)) { entityManager.remove(user); } else {
		 * entityManager.remove(entityManager.merge(user)); }
		 */
	}

	@SuppressWarnings("unchecked")
	public List<User> getUserByAge(int age) {
		return entityManager.createQuery("from User where age='" + age + "'").getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<User> getAgeInRange(int age1, int age2) {
		if (age2 < age1) {
			int temp = age2;
			age2 = age1;
			age1 = temp;
		}
		return entityManager.createNamedQuery("getAgeByRange").setParameter("age1", age1).setParameter("age2", age2)
				.getResultList();
	}
}
