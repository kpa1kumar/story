package com.zycus.integration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.zycus.integration.model.User;
import com.zycus.integration.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService useser;

	@Autowired
	private JmsTemplate jmsTemplate;

	@RequestMapping(value = "/user", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody User saveUser(@RequestBody User user) {
		// useser.save(user);
		/*
		 * String message=user.toString()+"saved";
		 * System.out.println("Sender:"+message);
		 */
		user.setHashcode(user.hashCode());
		jmsTemplate.convertAndSend("InsertQueue", user);
		User obj = (User) jmsTemplate.receiveAndConvert("InsertQueue");
		useser.save(obj);
		return obj;
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<User> users() {
		jmsTemplate.convertAndSend("PavanQueue", "Dislaying all Users");
		return useser.listAll();
	}

	/*
	 * @RequestMapping(value="/user/{iden}", method=RequestMethod.GET) public
	 * Resource<User> user(@PathVariable int iden) { User u=use.getUserById(iden);
	 * Resource<User> r=new Resource<User>(u);
	 * r.add(linkTo(methodOn(UserController.class).user(iden)).withSelfRel());
	 * return r; //return use.getUserById(iden); }
	 */

	@RequestMapping(value = "/user/{iden}", method = RequestMethod.GET)
	public User getUserById(@PathVariable int iden) {
		return useser.getById(iden);
	}

	@RequestMapping(value = "/user", method = RequestMethod.PUT)
	public @ResponseBody User updateUser(@RequestBody User user) {
		user.setHashcode(user.hashCode());
		useser.update(user);
		return user;
	}

	@RequestMapping(value = "/user/{iden}", method = RequestMethod.DELETE)
	public String DeleteUser(@PathVariable int iden) {
		/*
		 * useser.delete(iden); String message="User "+iden+" is deleted ";
		 * System.out.println("Sender:"+message);
		 */
		User u = new User(iden);
		jmsTemplate.convertAndSend("DeleteQueue", u);
		User user = (User) jmsTemplate.receiveAndConvert("DeleteQueue");
		System.out.println(user.getIden());
		useser.delete(user);
		return "User " + user.getIden() + " deleted Succesfully";
	}

	@RequestMapping(value = "/hashUser", method = RequestMethod.GET)
	public void hashUser(@ModelAttribute User user) {
		System.out.println(user.getIden() + "," + user.getName() + "," + user.getAge() + "," + user.getDept() + "==="
				+ user.hashCode());
	}

	@RequestMapping(value = "/age/{age}", method = RequestMethod.GET)
	public List<User> getByAge(@PathVariable int age) {
		System.out.println(age);
		return useser.getByAge(age);
	}

	@RequestMapping(value = "/age/{age1}/{age2}", method = RequestMethod.GET)
	public List<User> getByAgeInRange(@PathVariable int age1, @PathVariable int age2) {
		return useser.getByAgeInRange(age1, age2);
	}
}
