package com.zycus.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.zycus.integration.model.User;


public class UserHashCodeTests {

	@Test
	public void hashCodeTest(){
		User user=new User(1,"pavankumar",27,"IT");
		assertEquals(-1121766542, user.hashCode());
	}
		
}
