# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import datetime
import math
import requests
import re
from json import dumps
import json
from dateutil import parser
import pytz
from flask import app, request, Flask, jsonify, make_response
from xmljson import parker
from xml.etree.ElementTree import fromstring
from google_play_scraper import Sort, reviews
from tzlocal import get_localzone

import FinalVersion

app = Flask(__name__)

@app.route('/getReviewByCountries', methods=['GET'])
def citiAppFinal():
    range = request.args.get('range')
    fromDate = request.args.get('startDate')
    toDate = request.args.get('endDate')
    country = request.args.get('country')
    count = request.args.get('count')
    return FinalVersion.citiAppFinal(country,count,fromDate,toDate,range)

@app.route('/citibank-app-scrapper', methods=['GET'])
def citiApp():
    fromDate = request.args.get('startDate')
    toDate = request.args.get('endDate')
    filteredCountry = request.args.get('country')
    count=validateAndSetCount(request.args.get('count'))
    filteredMapData = {}
    mapData = {'sg': '370773317', 'bh': '504167405', 'sgipb': '463403661', 'hk': '373717372', 'in': '467987820',
               'my': '381059293', 'ae': '415169488', 'uk': '890420785', 'ru': '520758931', 'pl': '421359287',
               'tw': '380431092', 'id': '533078484', 'kr': '1179759666',
               'au': '394880260', 'ph': '484428618', 'vn': '435699106', 'cn': '444660435', 'th': '386285756'}
    if filteredCountry is None:
        filteredMapData = mapData
    else:
        filteredMapData = fillteredByCountry(mapData, filteredCountry)
    response = {}
    print(filteredMapData)
    for key in filteredMapData:
        print(key, filteredMapData[key])
        response[key] = storeScrapToFile(fromDate, toDate, key, filteredMapData[key],count)
    with open("extracted_all.json", "w", encoding='utf-8') as f:
        f.write(json.dumps(response, ensure_ascii=False))
    return response  # json.dumps(response, sort_keys=True, indent=4, separators=(',', ': '))

def validateAndSetCount(count):
    try:
        count = int(count)
    except:
        count = 100
    if isinstance(count, int) and count>100 and count<=1000:
        count = count / 100
        return math.ceil(count)+1
    else:
        print(count)
        return 2


def fillteredByCountry(mapData, fillteredCountry):
    keys = fillteredCountry.split(",")  # got dup
    print(keys)
    return {k: v for k, v in mapData.items() if k in keys}


def getRecordsByContinuationToken(continuation_token, country):
    try:
        appId = getAppId(country)
        result, token = reviews(
            appId,
            continuation_token=continuation_token
        )
        return token
    except:
        return 1


def getAppId(country):
    appId = 'com.citibank.mobile.' + country
    if (country == 'in'):
        appId = 'com.citiuat'
    if (country == 'my'):
        appId = 'com.citibank.CitibankMY'
    if (country == 'ph'):
        appId = 'com.citibank.CitibankPH'
    if (country == 'ae'):
        appId = 'com.citibank.mobile.citiuaePAT'
    if (country == 'pl'):
        appId = 'com.konylabs.cbplpat'
    if (country == 'th'):
        appId = 'com.mobile.co.th'
    if (country == 'kr'):
        appId = 'kr.co.citibank.citimobile'
    if (country == 'id'):
        appId = 'com.mobile.co.id'
    return appId


def callAppStoreApi(url):
    resp = requests.get(url)
    resp.encoding = 'utf-8-sig'
    print(url)
    if resp.status_code != 200:
        # This means something went wrong.
        print("something went wrong with Appstore API. Kindly check :)")
        raise resp.status_code
    else:
        return resp.text


def isFilterByDateRequired(startDate, endDate):
    if startDate is None or endDate is None:
        return False
    try:
        datetime.datetime.strptime(startDate, "%d-%m-%Y")
        datetime.datetime.strptime(endDate, "%d-%m-%Y")
        return True
    except ValueError:
        print("Issue with filtered date argument. Please pass valid date Format :)")
        return False


def filterJsonData(jsonData):
    keys = {"source", "text", "rating", "version", "date"}
    return [{k: v for k, v in i.items() if k in keys} for i in jsonData]


def myconverter(o):
    import datetime
    if isinstance(o, datetime.datetime):
        local_tz = get_localzone()
        datetime_obj_sg = parser.parse(str(o)).astimezone(local_tz).isoformat()
        date_ = parser.parse(str(datetime_obj_sg)).isoformat("T")
        return convertToTimeZone(date_)


def extractXmlAndConvertToJson(xmlResponse, country):
    xml = re.sub('<link rel[^<]+>', "", xmlResponse)
    xml = re.sub('<content type="html">([\s\S]*?)+</content>', "", xml)
    xml = "\n".join([ll.rstrip() for ll in xml.splitlines() if ll.strip()])

    jsonData = dumps(parker.data(fromstring(xml)))
    # with open("extractedxmlt.json", "w",encoding='utf-8') as f:
    #     f.write(json.dumps(jsonData,default=myconverter,ensure_ascii=False))
    jsonData = re.sub('{http://www.w3.org/2005/Atom}', "", jsonData)
    jsonData = re.sub('{http://itunes.apple.com/rss}', "", jsonData)
    loadJson = json.loads(jsonData)
    listOfJson = []
    if ('entry' in loadJson):
        print('yessssssss')
        listOfJson = loadJson['entry']
    else:
        print('noooooooooo')
        listOfJson = []

    # "updated","id","title","content","contentType","voteSum","voteCount","rating","version"
    # keys = {"updated", "rating", "title", "content", "version"}
    # finalData = [{k: v for k, v in i.items() if k in keys} for i in listOfJson]

    for key in listOfJson:
        key.update({'source': 'App Store'})
        key['date'] = convertToTimeZone(key['updated'])
        # key['date'] = key.pop('updated')
        # key['score'] = key.pop('rating')
        key['text'] = str(key['title']) + " " + str(key['content'])

    return filterJsonData(listOfJson)


def convertToTimeZone(dateValue):
    sg_tz = pytz.timezone('Asia/Singapore')
    timeZone = sg_tz
    return parser.parse(dateValue).astimezone(timeZone).isoformat()


def filterByDate(commentedDate, fromDate, toDate):
    extractDate = re.sub("T", " ", str(commentedDate)[0:19])
    commentedDate = datetime.datetime.strptime(extractDate, '%Y-%m-%d %H:%M:%S').date()
    start = fromDate
    end = toDate
    if start <= commentedDate <= end:
        print(commentedDate, "######")
        return True
    else:
        return False


def storeScrapToFile(fromDate, toDate, country, id,count):
    # AppStore begins
    appStoreData = []
    temp = country
    for i in range(1, count):
        if (country == 'uk'):
            country = 'gb'
        if (country == 'tw' or country == 'kr' or country == 'sgipb'):
            country = 'us'
        url = 'https://itunes.apple.com/' + country + '/rss/customerreviews/page=' + str(
            i) + '/id=' + id + '/sortby=mostrecent/xml'
        # print(url)
        country = temp
        xmlResponse = callAppStoreApi(url)
        response = extractXmlAndConvertToJson(xmlResponse, country)
        if (response == []):
            break
        else:
            appStoreData = appStoreData + response
    # AppStore ends
    # Play store begins
    appId = getAppId(country)
    if (country == 'sgipb'):
        temp = country
        country = 'sg'
    playStoreResult, continuation_token = reviews(
        appId,
        lang='en',  # defaults to 'en'
        country=country,  # defaults to 'us'
        sort=Sort.NEWEST,  # defaults to Sort.MOST_RELEVANT
        count=50,  # defaults to 100
        # filter_score_with=None # defaults to None(means all score)
    )
    country = temp
    for i in range(1, count-1):
        tokenObj = getRecordsByContinuationToken(continuation_token, country)
        if (tokenObj == 1):
            break
        else:
            result, token = reviews(
                appId,
                continuation_token=continuation_token)
            playStoreResult = playStoreResult + result
            continuation_token = tokenObj

    # rename key from content to comment
    for key in playStoreResult:
        key.update({'source': 'Play Store'})
        key['text'] = key.pop('content')
        key['date'] = myconverter(key['at'])
        key['rating'] = key.pop('score')
        key['version'] = key.pop('reviewCreatedVersion')

    playStoreData = filterJsonData(playStoreResult)

    citiAppData = playStoreData + appStoreData
    filteredResult = []
    # print(citiAppData)
    if isFilterByDateRequired(fromDate, toDate):
        fromDate = datetime.datetime.strptime(fromDate, "%d-%m-%Y").date()
        toDate = datetime.datetime.strptime(toDate, "%d-%m-%Y").date()
        if fromDate >= toDate:
            swap = fromDate
            fromDate = toDate
            toDate = swap
        filteredResult = [key for key in citiAppData if filterByDate(key['date'], fromDate, toDate)]
    else:
        filteredResult = citiAppData
    # print(filteredResult)
    # with open("extracteddummytest.json", "w", encoding='utf-8') as f:
    #     f.write(json.dumps(filteredResult, default=myconverter, ensure_ascii=False, sort_keys=True))
    return filteredResult


if __name__ == "__main__":
    from waitress import serve

    serve(app, host="0.0.0.0", port=5000)
